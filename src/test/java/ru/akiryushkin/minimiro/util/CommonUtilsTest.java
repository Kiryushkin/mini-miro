package ru.akiryushkin.minimiro.util;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.akiryushkin.minimiro.model.PointInfo;
import ru.akiryushkin.minimiro.model.RectangleInfo;

public class CommonUtilsTest {
    private final RectangleInfo rectangleInfo = new RectangleInfo(new PointInfo(0L, 0L), new PointInfo(100L , 150L));

    @DataProvider(name = "rectangleInRectangleData")
    public Object[][] rectangleInRectangleData() {
        return new Object[][] {
                //x, y, width, height
                { 1L, 1L, 1L, 1L },
                { 50L, 50L, 100L, 100L },
                { 50L, 50L, 100L, 100L },
                { 50L, 100L, 100L, 100L },
        };
    }

    @DataProvider(name = "rectangleNotInRectangleData")
    public Object[][] rectangleNotInRectangleData() {

        return new Object[][] {
                //x, y, width, height
                { 100L, 100L, 100L, 100L },
                { 200L, 2000L, 1500L, 1200L },
                { 200L, 2000L, 1500L, 1200L },
                { 0L, 0L, 0L, 0L },
        };
    }

    @Test(dataProvider = "rectangleInRectangleData")
    public void checkRectangleInRectangleTest(Long x, Long y, Long width, Long height) {
        Assert.assertTrue(CommonUtils.isRectangleContainsPartOfRectangle(rectangleInfo, x, y, width, height));
    }

    @Test(dataProvider = "rectangleNotInRectangleData")
    public void checkRectangleOutRectangleTest(Long x, Long y, Long width, Long height) {
        Assert.assertFalse(CommonUtils.isRectangleContainsPartOfRectangle(rectangleInfo, x, y, width, height));
    }
}
