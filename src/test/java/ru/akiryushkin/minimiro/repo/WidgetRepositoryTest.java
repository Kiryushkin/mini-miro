package ru.akiryushkin.minimiro.repo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.akiryushkin.minimiro.model.WidgetInfo;
import ru.akiryushkin.minimiro.service.Constants;
import ru.akiryushkin.minimiro.service.WidgetNotFoundException;

import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicLong;

@SpringBootTest
@Slf4j
public class WidgetRepositoryTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private WidgetRepository widgetRepository;

    private SecureRandom rnd;
    private List<WidgetInfo> expectedWidgets = Collections.synchronizedList(new ArrayList<>());
    private AtomicLong widgetEditCheckSum;

    @BeforeClass
    public void init() {
        rnd = new SecureRandom();
        expectedWidgets = Collections.synchronizedList(new ArrayList<>());
        widgetEditCheckSum = new AtomicLong(0);
    }

    @Test
    public void testValidZIndexLogic() {
        final WidgetInfo w1 = widgetRepository.add(1L, 1L, 1L, 1L, 1L);
        final WidgetInfo w2 = widgetRepository.add(2L, 2L, 2L, 2L, 10L);
        final WidgetInfo w3 = widgetRepository.add(2L, 2L, 2L, 2L, -2L);
        final WidgetInfo w4 = widgetRepository.add(2L, 2L, 2L, 2L, 10023L);
        final WidgetInfo w5 = widgetRepository.add(2L, 2L, 2L, 2L, 7L);

        final Long currentMaxZIndex = widgetRepository.getAll(null, null, null)
                .stream()
                .max(Comparator.comparingLong(WidgetInfo::getZIndex))
                .get()
                .getZIndex();
        final WidgetInfo widgetInfo = widgetRepository.add(2L, 2L, 2L, 2L, null);
        Assert.assertEquals(widgetInfo.getZIndex(), Long.valueOf(currentMaxZIndex + 1));
        expectedWidgets.add(widgetInfo);
        expectedWidgets.add(w1);
        expectedWidgets.add(w2);
        expectedWidgets.add(w3);
        expectedWidgets.add(w4);
        expectedWidgets.add(w5);
    }

    @Test(dependsOnMethods = "testValidZIndexLogic", threadPoolSize = 5, invocationCount = 300)
    public void testAddWidget() {
        final Long x = rnd.nextLong();
        final Long y = rnd.nextLong();
        final Long width = rnd.nextLong();
        final Long height = rnd.nextLong();
        final Long zIndex = rnd.nextLong();

        final WidgetInfo widgetInfo = widgetRepository.add(x, y, width, height, zIndex);
        final WidgetInfo expectedWidgetInfo = new WidgetInfo(widgetInfo.getUuid(), x, y, width, height, zIndex, new Date());

        Assert.assertEquals(expectedWidgetInfo.getX(), widgetInfo.getX());
        Assert.assertEquals(expectedWidgetInfo.getY(), widgetInfo.getY());
        Assert.assertEquals(expectedWidgetInfo.getWidth(), widgetInfo.getWidth());
        Assert.assertEquals(expectedWidgetInfo.getHeight(), widgetInfo.getHeight());
        expectedWidgets.add(expectedWidgetInfo);
    }

    @Test(dependsOnMethods = "testAddWidget")
    public void testGetWidget() {
        for (WidgetInfo widgetInfo : expectedWidgets) {
            final WidgetInfo actualWidgetInfo = widgetRepository.get(UUID.fromString(widgetInfo.getUuid()));
            Assert.assertEquals(widgetInfo.getX(), actualWidgetInfo.getX());
            Assert.assertEquals(widgetInfo.getY(), actualWidgetInfo.getY());
            Assert.assertEquals(widgetInfo.getWidth(), actualWidgetInfo.getWidth());
            Assert.assertEquals(widgetInfo.getHeight(), actualWidgetInfo.getHeight());
        }
    }

    @Test(dependsOnMethods = "testGetWidget")
    public void testEditWidgets() throws InterruptedException, ExecutionException {
        final ArrayList<FutureTask> futureTasks = new ArrayList<>();
        //test edit with run in different threads
        expectedWidgets.forEach(w -> futureTasks.add(new FutureTask<>(() -> {
                final Long newX = (long) rnd.nextInt();
                final Long newY = (long) rnd.nextInt();
                final Long newWidth = (long) rnd.nextInt();
                final Long newHeight = (long) rnd.nextInt();
                widgetRepository.edit(UUID.fromString(w.getUuid()), newX, newY, newWidth, newHeight, w.getZIndex());

                final WidgetInfo actualWidget = widgetRepository.get(UUID.fromString(w.getUuid()));

                Assert.assertEquals(newX, actualWidget.getX());
                Assert.assertEquals(newY, actualWidget.getY());
                Assert.assertEquals(newWidth, actualWidget.getWidth());
                Assert.assertEquals(newHeight, actualWidget.getHeight());
                // accumulate widget checksum as all fields of widget excluding z-index and uuid
                widgetEditCheckSum.addAndGet(newX + newY + newWidth + newHeight);
                return widgetEditCheckSum;
        })));

        futureTasks.forEach(t -> new Thread(t).start());
        // wait while all futures will complete
        for (FutureTask futureTask : futureTasks) {
            futureTask.get();
        }

        //get actual widget checksum and compare it with accumulated
        final Long actualWidgetCheckSum = widgetRepository.getAll(null, null, null)
                .stream()
                .mapToLong(w -> w.getX() + w.getY() + w.getWidth() + w.getHeight())
                .sum();
        Assert.assertEquals(widgetEditCheckSum.get(), actualWidgetCheckSum.longValue());

        //test get widgets by page
        final Integer itemsOnPageCount = Constants.MIN_GET_ALL_WIDGETS_PAGE_LIMIT;
        final Long page = 3L;
        final List<WidgetInfo> widgetInfosThirdPage = widgetRepository.getAll(page, itemsOnPageCount, null);
        Assert.assertEquals(widgetInfosThirdPage.size(), itemsOnPageCount.intValue());
        final List<WidgetInfo> allWidgetInfos = widgetRepository.getAll(null , null ,null);

        final Iterator widgetThirdPageIterator = widgetInfosThirdPage.iterator();
        allWidgetInfos.stream()
                .skip(itemsOnPageCount * (page - 1))
                .limit(itemsOnPageCount)
                .forEach(w -> Assert.assertEquals(w, widgetThirdPageIterator.next()));
    }

    @Test(dependsOnMethods = "testEditWidgets")
    public void testRemoveWidget() {
        final UUID uuidForRemove = UUID.fromString(expectedWidgets.get(0).getUuid());
        widgetRepository.remove(uuidForRemove);

        Assert.assertThrows(WidgetNotFoundException.class, () -> widgetRepository.get(uuidForRemove));
    }
}
