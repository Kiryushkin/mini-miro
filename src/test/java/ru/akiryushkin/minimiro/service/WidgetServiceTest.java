package ru.akiryushkin.minimiro.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.akiryushkin.minimiro.model.WidgetInfo;

import java.util.Date;
import java.util.UUID;

@SpringBootTest
@Slf4j
public class WidgetServiceTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private WidgetService widgetService;

    @Test
    public void testAddWidget() throws Exception {
        final WidgetInfo testInfo = new WidgetInfo(UUID.randomUUID().toString(), 1L, 2L, 3L, 4L, 5L, new Date());

        final WidgetInfo response = widgetService.addWidget(testInfo.getX(), testInfo.getY(), testInfo.getWidth(),
                testInfo.getHeight(), testInfo.getZIndex()).get().getWidget();

        Assert.assertEquals(response.getX(), testInfo.getX());
        Assert.assertEquals(response.getY(), testInfo.getY());
        Assert.assertEquals(response.getWidth(), testInfo.getWidth());
        Assert.assertEquals(response.getHeight(), testInfo.getHeight());
        Assert.assertEquals(response.getZIndex(), testInfo.getZIndex());
    }

    @Test
    @SuppressWarnings("AssertEqualsBetweenInconvertibleTypesTestNG")
    public void remove() throws Exception {
        final WidgetInfo testInfo = new WidgetInfo(UUID.randomUUID().toString(), 1L, 2L, 3L, 4L, 5L, new Date());
        final WidgetInfo retInfo = widgetService.addWidget(testInfo.getX(), testInfo.getY(), testInfo.getWidth(),
                testInfo.getHeight(), testInfo.getZIndex()).get().getWidget();
        try {
            widgetService.removeWidget(retInfo.getUuid()).get();
        } catch (Exception e) {
            Assert.assertEquals(e.getCause(), WidgetNotFoundException.class);
        }
    }
}
