package ru.akiryushkin.minimiro.service;


import ru.akiryushkin.minimiro.model.RectangleInfo;
import ru.akiryushkin.minimiro.controller.data.WidgetInfoListResponse;
import ru.akiryushkin.minimiro.controller.data.WidgetInfoResponse;

import java.util.concurrent.CompletableFuture;

public interface WidgetService {
    CompletableFuture<WidgetInfoResponse> addWidget(Long x, Long y, Long width, Long height, Long zIndex);

    CompletableFuture<WidgetInfoResponse> editWidget(String uuid, Long x, Long y, Long width, Long height, Long zIndex);

    CompletableFuture<WidgetInfoResponse> removeWidget(String uuid);

    CompletableFuture<WidgetInfoResponse> getWidget(String uuid);

    CompletableFuture<WidgetInfoListResponse> getAllWidgets(Long page, Integer countOnPage, RectangleInfo rectangleInfo);
}