package ru.akiryushkin.minimiro.service;

public class UnexpectedException extends RuntimeException {
    public UnexpectedException(String msg) {
        super(msg);
    }
}
