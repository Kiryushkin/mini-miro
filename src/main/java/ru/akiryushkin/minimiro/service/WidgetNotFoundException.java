package ru.akiryushkin.minimiro.service;

import lombok.Getter;

public class WidgetNotFoundException extends RuntimeException {
    @Getter
    final String detailMsg;

    public WidgetNotFoundException(String uuid) {
        this.detailMsg = String.format("Widget not found by uuid %s", uuid);
    }
}
