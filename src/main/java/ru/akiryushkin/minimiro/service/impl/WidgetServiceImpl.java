package ru.akiryushkin.minimiro.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.akiryushkin.minimiro.model.RectangleInfo;
import ru.akiryushkin.minimiro.controller.data.WidgetInfoListResponse;
import ru.akiryushkin.minimiro.controller.data.WidgetInfoResponse;
import ru.akiryushkin.minimiro.property.MiniMiroProperties;
import ru.akiryushkin.minimiro.repo.WidgetRepository;
import ru.akiryushkin.minimiro.service.BadRectangleException;
import ru.akiryushkin.minimiro.service.WidgetService;

import java.util.*;
import java.util.concurrent.CompletableFuture;

@Service
@Async(MiniMiroProperties.WIDGET_SERVICE_POOL_NAME)
public class WidgetServiceImpl implements WidgetService {
    private final WidgetRepository widgetRepository;

    @Autowired
    public WidgetServiceImpl(WidgetRepository widgetRepository) {
        this.widgetRepository = widgetRepository;
    }

    public CompletableFuture<WidgetInfoResponse> addWidget(Long x, Long y, Long width, Long height, Long zIndex) {
        return CompletableFuture
                .completedFuture(new WidgetInfoResponse(widgetRepository.add(x, y, width, height, zIndex)));
    }

    public CompletableFuture<WidgetInfoResponse> editWidget(String uuid, Long x, Long y, Long width, Long height, Long zIndex) {
        return CompletableFuture
                .completedFuture(new WidgetInfoResponse(widgetRepository.edit(UUID.fromString(uuid), x, y, width, height, zIndex)));
    }

    public CompletableFuture<WidgetInfoResponse> removeWidget(String uuid) {
        return CompletableFuture
                .completedFuture(new WidgetInfoResponse(widgetRepository.remove(UUID.fromString(uuid))));
    }


    public CompletableFuture<WidgetInfoResponse> getWidget(String uuid) {
        return CompletableFuture
                .completedFuture(new WidgetInfoResponse(widgetRepository.get(UUID.fromString(uuid))));
    }

    public CompletableFuture<WidgetInfoListResponse> getAllWidgets(Long page, Integer countOnPage, RectangleInfo rectangleInfo) {
        if (rectangleInfo != null) {
            if (rectangleInfo.getLeftTopPoint().getX() < rectangleInfo.getRightLowerPoint().getX() ||
                    rectangleInfo.getLeftTopPoint().getY() < rectangleInfo.getRightLowerPoint().getY()) {
                throw new BadRectangleException();
            }
        }

        return CompletableFuture
                .completedFuture(new WidgetInfoListResponse(widgetRepository.getAll(page, countOnPage, rectangleInfo)));
    }
}