package ru.akiryushkin.minimiro.service;

public class Constants {
    public static final int MIN_GET_ALL_WIDGETS_PAGE_LIMIT = 10;
    public static final int MAX_GET_ALL_WIDGETS_PAGE_LIMIT = 200;
}
