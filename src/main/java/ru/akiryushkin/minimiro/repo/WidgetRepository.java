package ru.akiryushkin.minimiro.repo;

import ru.akiryushkin.minimiro.model.RectangleInfo;
import ru.akiryushkin.minimiro.model.WidgetInfo;

import java.util.List;
import java.util.UUID;

public interface WidgetRepository {
    WidgetInfo get(UUID uuid);

    WidgetInfo add(Long x, Long y, Long width, Long height, Long zIndex);

    WidgetInfo edit(UUID uuid, Long x, Long y, Long width, Long height, Long zIndex);

    WidgetInfo remove(UUID uuid);

    List<WidgetInfo> getAll(Long page, Integer countOnPage, RectangleInfo rectangleInfo);
}
