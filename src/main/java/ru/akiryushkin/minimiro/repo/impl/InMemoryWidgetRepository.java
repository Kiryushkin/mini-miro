package ru.akiryushkin.minimiro.repo.impl;

import org.springframework.stereotype.Repository;
import ru.akiryushkin.minimiro.model.RectangleInfo;
import ru.akiryushkin.minimiro.model.Widget;
import ru.akiryushkin.minimiro.model.WidgetInfo;
import ru.akiryushkin.minimiro.repo.WidgetRepository;
import ru.akiryushkin.minimiro.service.BadRectangleException;
import ru.akiryushkin.minimiro.service.Constants;
import ru.akiryushkin.minimiro.service.UnexpectedException;
import ru.akiryushkin.minimiro.util.CommonUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class InMemoryWidgetRepository implements WidgetRepository {
    private final ConcurrentHashMap<UUID, Widget> widgetMap = new ConcurrentHashMap<>();

    @Override
    public WidgetInfo get(UUID uuid) {
        final Widget widget = widgetMap.get(uuid);
        return WidgetInfo.fromWidgetCheckNull(uuid, widget);
    }

    @Override
    public synchronized WidgetInfo add(Long x, Long y, Long width, Long height, Long zIndex) {
            final UUID uuid = UUID.randomUUID();
            final Widget widget = new Widget(x, y, width, height, getZIndex(zIndex));

            if (widgetMap.putIfAbsent(uuid, widget) != null) {
                throw new UnexpectedException("Incredible happened: uuid collision");
            }

            incOtherZIndexesIfNeed(uuid, zIndex);
            return WidgetInfo.fromWidgetCheckNull(uuid, widget);
    }

    @Override
    public synchronized WidgetInfo edit(UUID uuid, Long x, Long y, Long width, Long height, Long zIndex) {
            final Widget widget = widgetMap.computeIfPresent(uuid, (k, v) -> {
                final Widget newWidget = new Widget();
                newWidget.setX(x == null ? v.getX() : x);
                newWidget.setY(y == null ? v.getY() : y);
                newWidget.setWidth(width == null ? v.getWidth() : width);
                newWidget.setHeight(height == null ? v.getHeight() : height);
                newWidget.setZIndex(getZIndex(zIndex));
                newWidget.setLastModifiedDate(new Date());
                return newWidget;
            });

            incOtherZIndexesIfNeed(uuid, zIndex);
            return WidgetInfo.fromWidgetCheckNull(uuid, widget);
    }

    @Override
    public WidgetInfo remove(UUID uuid) {
        final Widget widget = widgetMap.remove(uuid);
        return WidgetInfo.fromWidgetCheckNull(uuid, widget);
    }

    @Override
    public List<WidgetInfo> getAll(Long page, Integer countOnPage, RectangleInfo rectangleInfo) {
        Stream<Map.Entry<UUID, Widget>> widgetsStream = widgetMap.entrySet()
                .stream();

        if (rectangleInfo != null) {
            widgetsStream = widgetsStream
                    .filter(w -> CommonUtils.isRectangleContainsPartOfRectangle(rectangleInfo,
                            w.getValue().getX(), w.getValue().getY(), w.getValue().getWidth(), w.getValue().getHeight()));
        }

        widgetsStream = widgetsStream.sorted(Comparator.comparingLong(w -> w.getValue().getZIndex()));

        if (page != null) {
            final Integer maxCountOnPage = countOnPage == null ? Constants.MIN_GET_ALL_WIDGETS_PAGE_LIMIT : countOnPage;
            final Long elementsForSkip = (page - 1L) * maxCountOnPage;
            widgetsStream = widgetsStream.skip(elementsForSkip)
                    .limit(maxCountOnPage);
        }

        final List<WidgetInfo> ret = widgetsStream.map(w -> WidgetInfo.fromWidgetCheckNull(w.getKey(), w.getValue()))
                .collect(Collectors.toList());
        return ret;
    }

    /**
     * Method gets an z-index based on the current widget z-indexes and, if necessary, updates their z-indexes.
     * Run only with with write lock.
     *
     * @param zIndex z-index from request, may be null
     * @return z-index for new widget
     */
    private Long getZIndex(Long zIndex) {
        //if the index is not specified, then take the increment of the maximum current index
        if (zIndex == null) {
            if (widgetMap.isEmpty()) {
                return Long.MIN_VALUE;
            } else {
                final Long maxZIndex = widgetMap.entrySet()
                        .stream()
                        .max(Comparator.comparingLong(s -> s.getValue().getZIndex()))
                        .get()
                        .getValue()
                        .getZIndex();

                return maxZIndex + 1;
            }
        } else {
            return zIndex;
        }
    }

    /**
     * Method increments all widget z-indexes (excluding new widget) if widget z-index is equivalent to new.
     * Run only with write lock.
     *
     * @param uuid   widget identifier that is updated / added
     * @param zIndex z-index from request, may be null
     */
    private void incOtherZIndexesIfNeed(UUID uuid, Long zIndex) {
        if (zIndex != null) {
            widgetMap.entrySet()
                    .stream()
                    .filter(w -> !w.getKey().equals(uuid) && w.getValue().getZIndex() >= zIndex)
                    .forEach(w -> w.getValue().setZIndex(w.getValue().getZIndex() + 1));
        }
    }
}
