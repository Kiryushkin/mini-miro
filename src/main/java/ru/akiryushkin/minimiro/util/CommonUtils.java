package ru.akiryushkin.minimiro.util;

import ru.akiryushkin.minimiro.model.RectangleInfo;

public class CommonUtils {
    public static boolean isRectangleContainsPartOfRectangle(RectangleInfo ri, Long xLeft, Long yLow, Long width, Long height) {
        final Long x1 = ri.getLeftTopPoint().getX();
        final Long x2 = ri.getRightLowerPoint().getX();
        final Long y1 = ri.getLeftTopPoint().getY();
        final Long y2 = ri.getRightLowerPoint().getY();

        final Long yTop = yLow + height;
        final Long xRight = xLeft + width;

        return xLeft > x1 && xLeft < x2 && yLow > y1 && yLow < y2 ||
                xLeft > x1 && xLeft < x2 && yTop > y1 && yTop < y2 ||
                xRight > x1 && xRight < x2 && yTop > y1 && yTop < y2 ||
                xRight > x1 && xRight < x2 && yLow > y1 && yLow < y2;
    }
}
