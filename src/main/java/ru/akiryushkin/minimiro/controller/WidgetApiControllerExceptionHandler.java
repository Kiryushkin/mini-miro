package ru.akiryushkin.minimiro.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.akiryushkin.minimiro.controller.data.ErrorResponse;
import ru.akiryushkin.minimiro.controller.data.OutApiError;
import ru.akiryushkin.minimiro.service.BadRectangleException;
import ru.akiryushkin.minimiro.service.WidgetNotFoundException;

import java.util.stream.Collectors;

@RestControllerAdvice(assignableTypes = WidgetApiController.class)
@ResponseBody
public class WidgetApiControllerExceptionHandler extends ResponseEntityExceptionHandler {
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(MissingServletRequestParameterException e,
                                                                          HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new ErrorResponse(OutApiError.INVALID_REQUEST, e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotAcceptable(HttpMediaTypeNotAcceptableException e,
                                                                      HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new ErrorResponse(OutApiError.INVALID_REQUEST, e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException e,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        return new ResponseEntity<>(new ErrorResponse(OutApiError.INVALID_REQUEST, e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ErrorResponse handleExceptions(Exception e, WebRequest request) {
        if (e instanceof MethodArgumentNotValidException) {
            final String detailInfo = ((MethodArgumentNotValidException) e).getBindingResult().getAllErrors().stream().map(error -> {
                if (error instanceof FieldError) {
                    return "Field: " + ((FieldError) error).getField() +
                            ", Value: " + ((FieldError) error).getRejectedValue() +
                            ", Message: " + error.getDefaultMessage();
                }
                if (error instanceof ObjectError) {
                    return "Object " + error.getObjectName() +
                            ", Message: " + error.getDefaultMessage();
                }

                return error.getDefaultMessage();
            }).collect(Collectors.joining("; "));

            return new ErrorResponse(OutApiError.INVALID_REQUEST, detailInfo);
        }

        if (e instanceof BadRectangleException) {
            return new ErrorResponse(OutApiError.BAD_RECTANGLE);
        }
        if (e.getCause() instanceof BadRectangleException) {
            return new ErrorResponse(OutApiError.BAD_RECTANGLE);
        }
        if (e.getCause() instanceof WidgetNotFoundException) {
            return new ErrorResponse(OutApiError.WIDGET_NOT_FOUND, ((WidgetNotFoundException) e).getDetailMsg());
        }
        if (e instanceof WidgetNotFoundException) {
            return new ErrorResponse(OutApiError.WIDGET_NOT_FOUND, ((WidgetNotFoundException) e).getDetailMsg());
        }

        return new ErrorResponse(OutApiError.SYSTEM_ERROR, e.getMessage());
    }
}
