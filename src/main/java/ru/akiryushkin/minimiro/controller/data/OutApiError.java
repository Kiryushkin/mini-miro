package ru.akiryushkin.minimiro.controller.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Applied error codes (to difference errors on client side)
 */
@AllArgsConstructor
public enum OutApiError {
    WIDGET_NOT_FOUND(100, "Widget not found"),

    BAD_RECTANGLE(101, "Bad rectangle top left and lower right points"),

    INVALID_REQUEST(302,"Bad request format"),

    SYSTEM_ERROR(999, "System error");

    @Getter
    final Integer code;
    @Getter
    final String message;
}
