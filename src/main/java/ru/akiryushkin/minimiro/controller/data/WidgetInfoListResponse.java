package ru.akiryushkin.minimiro.controller.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.akiryushkin.minimiro.model.WidgetInfo;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
public class WidgetInfoListResponse {
    @NotNull
    @Valid
    final List<WidgetInfo> widgets;
}
