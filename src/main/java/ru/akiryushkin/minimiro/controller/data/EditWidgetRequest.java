package ru.akiryushkin.minimiro.controller.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.akiryushkin.minimiro.controller.data.validation.ValidUUID;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class EditWidgetRequest {
    @NotNull
    @ValidUUID
    private String uuid;

    private Long x;

    private Long y;

    private Long width;

    private Long height;

    private Long zIndex;
}
