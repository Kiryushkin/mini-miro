package ru.akiryushkin.minimiro.controller.data;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
abstract class BaseResponseInfo {
    @NotNull
    private final Integer errorCode;
    /**
     * Message for end client
     */
    private final String errorMsg;
    /**
     * Message for log in integrating service
     */
    private final String detailedErrorMsg;

    BaseResponseInfo(OutApiError  error) {
        this.errorCode = error.getCode();
        this.errorMsg = error.getMessage();
        this.detailedErrorMsg = "";
    }

    BaseResponseInfo(OutApiError  error, String detailedMsg) {
        this.errorCode = error.getCode();
        this.errorMsg = error.getMessage();
        this.detailedErrorMsg = detailedMsg;
    }
}
