package ru.akiryushkin.minimiro.controller.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.akiryushkin.minimiro.controller.data.validation.ValidUUID;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class GetWidgetRequest {
    @NotNull
    @ValidUUID
    private String uuid;
}
