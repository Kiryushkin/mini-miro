package ru.akiryushkin.minimiro.controller.data;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ErrorResponse {
    @NotNull
    private final Integer errorCode;
    /**
     * Message for end client
     */
    private final String errorMsg;
    /**
     * Message for log in integrating service
     */
    private final String detailedErrorMsg;

    public ErrorResponse(OutApiError  error) {
        this.errorCode = error.getCode();
        this.errorMsg = error.getMessage();
        this.detailedErrorMsg = "";
    }

    public ErrorResponse(OutApiError  error, String detailedMsg) {
        this.errorCode = error.getCode();
        this.errorMsg = error.getMessage();
        this.detailedErrorMsg = detailedMsg;
    }
}
