package ru.akiryushkin.minimiro.controller.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class AddWidgetRequest {
    @NotNull
    private Long x;

    @NotNull
    private Long y;

    @NotNull
    private Long width;

    @NotNull
    private Long height;

    private Long zIndex;
}
