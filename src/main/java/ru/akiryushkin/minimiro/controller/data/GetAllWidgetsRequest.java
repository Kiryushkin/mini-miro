package ru.akiryushkin.minimiro.controller.data;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.akiryushkin.minimiro.model.RectangleInfo;
import ru.akiryushkin.minimiro.service.Constants;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@NoArgsConstructor
public class GetAllWidgetsRequest {
    @Min(1)
    private Long page;

    @Min(Constants.MIN_GET_ALL_WIDGETS_PAGE_LIMIT)
    @Max(Constants.MAX_GET_ALL_WIDGETS_PAGE_LIMIT)
    private Integer limit;

    @Valid
    private RectangleInfo rectangleInfo;
}
