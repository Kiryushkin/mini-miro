package ru.akiryushkin.minimiro.controller.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.akiryushkin.minimiro.model.WidgetInfo;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
public class WidgetInfoResponse {
    @NotNull
    @Valid
    final WidgetInfo widget;
}
