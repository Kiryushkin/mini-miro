package ru.akiryushkin.minimiro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.akiryushkin.minimiro.controller.data.*;
import ru.akiryushkin.minimiro.service.WidgetService;

import javax.validation.Valid;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/api/widget")
public class WidgetApiController {
    private final WidgetService widgetService;

    @Autowired
    public WidgetApiController(WidgetService widgetService) {
        this.widgetService = widgetService;
    }

    @RequestMapping(value = "/add",
                 produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
                 consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
                 method = RequestMethod.POST)
    public CompletableFuture<WidgetInfoResponse> addWidget(@RequestBody @Valid AddWidgetRequest request) {
        return widgetService.addWidget(request.getX(),
                                       request.getY(),
                                       request.getWidth(),
                                       request.getHeight(),
                                       request.getZIndex());
    }

    @RequestMapping(value = "/edit",
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
                    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
                    method = RequestMethod.POST)
    public CompletableFuture<WidgetInfoResponse> editWidget(@RequestBody @Valid EditWidgetRequest request) {
        return widgetService.editWidget(request.getUuid(),
                                        request.getX(),
                                        request.getY(),
                                        request.getWidth(),
                                        request.getHeight(),
                                        request.getZIndex());
    }

    @RequestMapping(value = "/remove",
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
                    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
                    method = RequestMethod.POST)
    public CompletableFuture<WidgetInfoResponse> removeWidget(@RequestBody @Valid RemoveWidgetRequest request) {
        return widgetService.removeWidget(request.getUuid());
    }

    @RequestMapping(value = "/get",
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
                    consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
                    method = RequestMethod.POST)
    public CompletableFuture<WidgetInfoResponse> getWidget(@RequestBody @Valid GetWidgetRequest request) {
        return widgetService.getWidget(request.getUuid());
    }

    @RequestMapping(value = "/get_all",
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
                    method = RequestMethod.POST)
    public CompletableFuture<WidgetInfoListResponse> getAllWidgets(@RequestBody(required = false) @Valid GetAllWidgetsRequest request) {
        return widgetService.getAllWidgets(request == null ? null : request.getPage(),
                                           request == null ? null : request.getLimit(),
                                           request == null ? null : request.getRectangleInfo());
    }
}
