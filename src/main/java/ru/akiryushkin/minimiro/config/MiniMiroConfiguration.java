package ru.akiryushkin.minimiro.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import ru.akiryushkin.minimiro.property.MiniMiroProperties;

@Configuration
@EnableAsync
@Slf4j
public class MiniMiroConfiguration {
    @Bean(MiniMiroProperties.WIDGET_SERVICE_POOL_NAME)
    public TaskExecutor widgetServiceTaskExecutor(final MiniMiroProperties properties) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(properties.getWidgetServicePool().getCorePoolSize());
        executor.setMaxPoolSize(properties.getWidgetServicePool().getMaxPoolSize());
        executor.setQueueCapacity(properties.getWidgetServicePool().getQueueCapacity());
        return executor;
    }
}
