package ru.akiryushkin.minimiro.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import ru.akiryushkin.minimiro.property.MiniMiroProperties;

@Configuration
@Profile(MiniMiroProperties.DEV_PROFILE)
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(WebSecurity webSecurity) {
        /*in dev profile spring security for widget api disabled for easy testing it.*/
        webSecurity.ignoring()
                .antMatchers("/api/widget/**");
    }
}

