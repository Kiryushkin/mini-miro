package ru.akiryushkin.minimiro.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
@Slf4j
public class LoggingAspect {

    @Pointcut("execution(public * ru.akiryushkin.minimiro.controller.WidgetApiController.*(..))")
    public void controllerMethods() {
    }

    @Pointcut ("execution(public * ru.akiryushkin.minimiro.controller.WidgetApiControllerExceptionHandler.*(..))")
    public void handlerExceptionMethods() {
    }

    @Before("controllerMethods()")
    public void logRequestMethod(JoinPoint joinPoint) {
        log.debug("request: {}", joinPoint.getArgs());
    }

    @Before ("handlerExceptionMethods()")
    public void logRequestExceptionMethod(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        /*1st arg is error, log this arg*/
        if (args[0]!= null){
            log.error("handle exception: {}", args[0]);
        }
    }
}