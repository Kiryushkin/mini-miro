package ru.akiryushkin.minimiro.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Widget {
    @NotNull
    private Long x;

    @NotNull
    private Long y;

    @NotNull
    private Long width;

    @NotNull
    private Long height;

    @NotNull
    private Long zIndex;

    @NotNull
    private Date lastModifiedDate;

    public Widget(Long x, Long y, Long width, Long height, Long zIndex) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.zIndex = zIndex;
        this.lastModifiedDate = new Date();
    }
}
