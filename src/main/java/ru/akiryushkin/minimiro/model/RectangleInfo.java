package ru.akiryushkin.minimiro.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class RectangleInfo {
    @NotNull
    private final PointInfo leftTopPoint;
    @NotNull
    private final PointInfo rightLowerPoint;
}

