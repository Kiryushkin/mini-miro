package ru.akiryushkin.minimiro.model;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class PointInfo {
    @NotNull
    private final Long x;
    @NotNull
    private final Long y;
}