package ru.akiryushkin.minimiro.model;

import lombok.*;
import ru.akiryushkin.minimiro.service.WidgetNotFoundException;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@ToString
@AllArgsConstructor
@EqualsAndHashCode
public class WidgetInfo {
    @NotNull
    final String uuid;

    @NotNull
    final Long x;

    @NotNull
    final Long y;

    @NotNull
    final Long width;

    @NotNull
    final Long height;

    final Long zIndex;

    @NotNull
    final Date lastModifiedDate;

    public static WidgetInfo fromWidgetCheckNull(UUID uuid, Widget widget) {
        if (widget == null) {
            throw new WidgetNotFoundException(uuid.toString());
        }
        return new WidgetInfo(uuid.toString(),
                widget.getX(),
                widget.getY(),
                widget.getWidth(),
                widget.getHeight(),
                widget.getZIndex(),
                widget.getLastModifiedDate());
    }
}