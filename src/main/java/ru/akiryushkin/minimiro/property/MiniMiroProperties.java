package ru.akiryushkin.minimiro.property;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Component
@Validated
@ConfigurationProperties(prefix = MiniMiroProperties.CONFIGURATION_PREFIX, ignoreUnknownFields = false)
@Data
@NoArgsConstructor
public class MiniMiroProperties {
    public static final String CONFIGURATION_PREFIX = "application";

    public static final String APPLICATION_NAME = "mini-miro";

    public static final String WIDGET_SERVICE_POOL_NAME = "widgetServicePool";

    public static final String DEV_PROFILE = "dev";

    @NotNull
    private ThreadPoolTaskExecutorProperties widgetServicePool;
}